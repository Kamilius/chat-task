﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DuplexChat
{
  public interface IChatCallback
  {
    [OperationContract(IsOneWay=true)]
    void MessageNotification(string msg);

    [OperationContract(IsOneWay = true)]
    void UserJoinedNotification(string name);

    [OperationContract(IsOneWay = true)]
    void UserLeftNotification(string name);
  }
}