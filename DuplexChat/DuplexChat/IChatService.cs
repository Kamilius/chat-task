﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DuplexChat
{
  [ServiceContract(Name="DuplexChat", SessionMode=SessionMode.Required, CallbackContract=typeof(IChatCallback))]
  public interface IChatService
  {
    [OperationContract(IsOneWay=true)]
    void RegisterUser(string username);

    [OperationContract(IsOneWay = true)]
    void RemoveUser(string username);

    [OperationContract(IsOneWay = true)]
    void SendMessage(string username, string message);    
  }
}
