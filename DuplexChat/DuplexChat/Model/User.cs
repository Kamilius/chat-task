﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DuplexChat.Model
{
  [DataContract]
  public class User
  {
    [DataMember]
    public String Name { get; set; }
    [DataMember]
    public IChatCallback Callback { get; set; }
  }
}