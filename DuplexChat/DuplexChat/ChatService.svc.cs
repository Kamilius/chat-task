﻿using DuplexChat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DuplexChat
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ChatService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select ChatService.svc or ChatService.svc.cs at the Solution Explorer and start debugging.
  public class ChatService : IChatService
  {
    private static List<User> _users = new List<User>();

    public void RegisterUser(string username)
    {
      IChatCallback callback = OperationContext.Current.GetCallbackChannel<IChatCallback>();

      if (callback != null)
      {
        //foreach (var user in _users)
        //{
        //  callback.UserJoinedNotification(String.Format("{0} has joined a chat.", username));
        //}
        lock (_users)
        {
          _users.Add(new User{
            Name = username,
            Callback = callback  
          });
        }
        foreach (var user in _users)
        {
          user.Callback.UserJoinedNotification(String.Format("{0} has joined a chat.", username));
        }
      }
    }

    public void RemoveUser(string username)
    {
      IChatCallback callback = OperationContext.Current.GetCallbackChannel<IChatCallback>();

      var leavingUser = _users.FirstOrDefault(x => x.Name == username);

      if (callback != null && leavingUser != null)
      {
        foreach (var user in _users)
        {
          user.Callback.UserLeftNotification(String.Format("{0} has left chat.", username));
        }
        lock (_users)
        {
          _users.Remove(leavingUser);
        }
      }
    }
    
    public void SendMessage(string username, string message)
    {
      IChatCallback callback = OperationContext.Current.GetCallbackChannel<IChatCallback>();

      if (callback != null)
      {
        foreach (var user in _users)
        {
          user.Callback.MessageNotification(String.Format("{0}: {1}", username, message));
        }
      }
    }
  }
}
