﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Chat.ServiceReference1;

namespace Chat
{
    [CallbackBehavior(ConcurrencyMode=ConcurrencyMode.Single, UseSynchronizationContext=false)]
    class ChatClient : DuplexChatCallback
    {
        public String Name = "";
        public DuplexChatClient _chatService = null;

        public ChatClient()
        {
            _chatService = new DuplexChatClient(new InstanceContext(this), "WSDualHttpBinding_DuplexChat");
            _chatService.Open();

            Console.Write("Please, enter your nickname: ");
            Name = Console.ReadLine();
            _chatService.RegisterUser(Name);
        }

        public void MessageNotification(string msg)
        {
            Console.WriteLine(msg);
        }

        public void UserJoinedNotification(string msg)
        {
            Console.WriteLine(msg);
        }

        public void UserLeftNotification(string msg)
        {
            Console.WriteLine(msg);
        }

        public void PostMessage(string msg)
        {
            _chatService.SendMessage(Name, msg);
        }

        public void QuitChat()
        {
            _chatService.RemoveUser(Name);
        }
    }
}
