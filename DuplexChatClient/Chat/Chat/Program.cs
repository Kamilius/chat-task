﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chat
{
    class Program
    {
        static void Main(string[] args)
        {
            ChatClient client = new ChatClient();
            String msg = "";
            while (true)
            {
                msg = Console.ReadLine().ToLower().Trim();
                if(msg == "exit" || msg == "quit")
                {
                    client.QuitChat();
                    Console.WriteLine("Good bye.");
                    break;
                }
                else
                {
                    client.PostMessage(msg);
                    Thread.Sleep(1);
                }
            }
        }
    }
}
